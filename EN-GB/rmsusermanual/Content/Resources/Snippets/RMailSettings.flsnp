﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:lastBlockDepth="7" MadCap:lastHeight="717" MadCap:lastWidth="780">
    <head>
    </head>
    <body>
        <table>
            <thead>
                <tr>
                    <td> 					Setting
					</td>
                    <td> 					Description
					</td>
                    <td> 					Instructions
					</td>
                </tr>
            </thead>
            <tr>
                <td><![CDATA[
					]]><span class="uiLabel">Status</span><![CDATA[
				]]></td>
                <td>The status of the <MadCap:variable name="UITextNames_main.rmail" />. For more information on <MadCap:variable name="UITextNames_main.rmail" /> statuses, see <MadCap:xref href="../../GlobalIchiba_RMSManual/ShopManual/Marketing/RMailStatuses.htm" class="xref_screen"> <MadCap:variable name="UITextNames_main.rmail" /> Statuses </MadCap:xref></td>
                <td>You cannot edit this field directly. See <MadCap:xref href="../../GlobalIchiba_RMSManual/ShopManual/Marketing/RMailStatuses.htm" class="xref_screen"><MadCap:variable name="UITextNames_main.rmail" /> Statuses </MadCap:xref>to learn how <MadCap:variable name="UITextNames_main.rmail" /> statuses change.</td>
            </tr>
            <tr>
                <td><span class="uiLabel">Recipients</span>
                </td>
                <td>The mailing list of recipients to receive the <MadCap:variable name="UITextNames_main.rmail" />. </td>
                <td>The default selection is <span class="uiLabel">All Subscribers</span>. To change the recipients, click <span class="uiLabel">Edit</span> and select a different mailing list. <br /><br />To learn more about creating custom mailing lists to send targeted<MadCap:variable name="UITextNames_main.rmail" />, see <MadCap:xref href="../../GlobalIchiba_RMSManual/ShopManual/Marketing/MailingLists.htm" class="xref_screen">Mailing Lists</MadCap:xref>.<br /><br /><p class="note"><span class="uiLabel">Note:</span>&#160;If a mailing list has no subscribers or is currently updating, it cannot be selected. If the mailing list is updating, wait for the update to complete, then select the desired mailing list.</p></td>
            </tr>
            <tr>
                <td><span class="uiLabel">Subject</span>
                </td>
                <td>The subject line of the email. The subject is visible to customers when they receive the email.</td>
                <td>Enter a subject line to describe the contents of your email. <br /></td>
            </tr>
            <tr>
                <td><span class="uiLabel">Type</span>
                </td>
                <td>The mode in which you will compose the email. <br /><br />In <span class="uiLabel">HTML Editor</span> mode, you can write your own HTML code for your email. This mode allows greater customisation in the look of your email, but requires that you know HTML. <br /><br />In <span class="uiLabel">HTML&#160;Template</span> mode, you can easily compose your email by adding images and text to a template provided by Rakuten. This mode does not require any knowledge of HTML. </td>
                <td>If you prefer to write your own HTML code, or would like to write a short, text-only email, select <span class="uiLabel">HTML Editor</span>.  <br /><br />If you prefer to compose your email without writing HTML code, select <span class="uiLabel">HTML Template</span>. </td>
            </tr>
            <tr>
                <td><span class="uiLabel">Body</span>
                </td>
                <td>The body of your email.</td>
                <td><b>HTML Editor:</b> <br />Click on the text between the header and footer to enter the body of the email. You can include HTML tags. Refer to <MadCap:xref href="../../GlobalIchiba_RMSManual/Appendices/HTMLTags/AllowedHTMLTags.htm" class="xref_screen">Allowed HTML Tags</MadCap:xref> for a list of HTML&#160;tags that you can include in your email.<br /><br /><b>HTML Template:</b><ul><li class="noSpace">Click on text areas to add text to your email. </li><li class="noSpace">Click on <span class="uiLabel">Add Top Image</span> to add a main image to your email.</li><li class="noSpace">Click on <span class="uiLabel">Add Product</span> to select a product to feature with a large image in your email.</li><li class="noSpace">Click on <span class="uiLabel">Add Products</span> to select up to 21 products to feature with smaller images in your email.</li></ul></td>
            </tr>
        </table>
    </body>
</html>