﻿<?xml version="1.0" encoding="utf-8"?>
<CatapultToc Version="1">
  <TocEntry Title="TitlePage" Link="/Content/GlobalIchiba_RMSManual/ShopManual/TitlePage.htm" conditions="display.PrintOnly"/>
  <TocEntry Title="MainTOC" Link="/Content/GlobalIchiba_RMSManual/ShopManual/MainTOC.htm" conditions="display.PrintOnly"/>
  <TocEntry Title="Getting Started with RMS" Link="/Content/GlobalIchiba_RMSManual/ShopManual/IntroductionToRakutenRMS/GettingStartedTOC.htm">
    <TocEntry Title="Introduction to RMSg" Link="/Content/GlobalIchiba_RMSManual/ShopManual/IntroductionToRakutenRMS/IntroductionToRakutenRMS.htm"/>
    <TocEntry Title="Getting Started Checklist" Link="/Content/GlobalIchiba_RMSManual/ShopManual/IntroductionToRakutenRMS/GettingStartedChecklist.htm"></TocEntry>
    <TocEntry Title="Manage Shop Status" Link="/Content/GlobalIchiba_RMSManual/ShopManual/IntroductionToRakutenRMS/ShopStatus.htm"></TocEntry>
    <TocEntry Title="Select Language" Link="/Content/GlobalIchiba_RMSManual/ShopManual/IntroductionToRakutenRMS/languageSelection.htm"></TocEntry>
    <TocEntry Title="Enable Two-Step Verification" Link="/Content/GlobalIchiba_RMSManual/ShopManual/IntroductionToRakutenRMS/two-stepVerification.htm"/>
    <TocEntry Title="Support" Link="/Content/GlobalIchiba_RMSManual/ShopManual/IntroductionToRakutenRMS/MessagesFromRakutenSupport.htm"/>
    <TocEntry Title="What&apos;s New?" Link="/Content/GlobalIchiba_RMSManual/ShopManual/IntroductionToRakutenRMS/WhatsNew.htm"/>
  </TocEntry>
  <TocEntry Title="[%=UITextNames_main.shippingPaymentSection%] Options" Link="/Content/GlobalIchiba_RMSManual/ShopManual/ShippingOptions/ShippingPaymentOptionsTOC.htm">
    <TocEntry Title="Manage Payment Options" Link="/Content/GlobalIchiba_RMSManual/ShopManual/PaymentOptions/PaymentOptions.htm" conditions="inc-feat.paymentOptions"/>
    <TocEntry Title="Manage Shipping Options" Link="/Content/GlobalIchiba_RMSManual/ShopManual/ShippingOptions/ShippingOptions.htm"/>
    <TocEntry Title="Add Shipping Options" Link="/Content/GlobalIchiba_RMSManual/ShopManual/ShippingOptions/AddEditShippingOptions.htm"/>
    <TocEntry Title="Edit Shipping Options" Link="/Content/GlobalIchiba_RMSManual/ShopManual/ShippingOptions/EditShippingOptions.htm"/>
    <TocEntry Title="Delete Shipping Options" Link="/Content/GlobalIchiba_RMSManual/ShopManual/ShippingOptions/DeleteShippingOptions.htm"/>
  </TocEntry>
  <TocEntry Title="Product Management" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Products/ProductManagementTOC.htm">
    <TocEntry Title="Introduction to Product Management" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Products/Products.htm"/>
    <TocEntry Title="Add Products" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Products/AddProducts.htm"/>
    <TocEntry Title="Edit Products" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Products/EditProducts.htm"/>
    <TocEntry Title="Delete Products" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Products/DeleteProducts.htm"/>
    <TocEntry Title="Update Product Quantities" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Products/UpdateQuantity.htm"/>
    <TocEntry Title="Upload Multiple Product Images" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Products/UploadMultipleImages.htm"/>
    <TocEntry Title="Create Product Videos" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Products/CreateVideos.htm"></TocEntry>
    <TocEntry Title="Add Product Videos" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Products/Videos.htm"/>
    <TocEntry Title="Featured Products" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Products/FeaturedProducts.htm"/>
    <TocEntry Title="Download Products Data" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Products/DownloadProductCSV.htm" conditions="display.ScreenOnly"/>
    <TocEntry Title="Bulk Product Updates" Link="/Content/GlobalIchiba_RMSManual/Appendices/CreatingCSVFiles/BulkProduct.htm" conditions="display.ScreenOnly"/>
    <TocEntry Title="Introduction to Shop Categories" Link="/Content/GlobalIchiba_RMSManual/ShopManual/ShopCategories/ShopCategories.htm"/>
    <TocEntry Title="Add Shop Categories" Link="/Content/GlobalIchiba_RMSManual/ShopManual/ShopCategories/AddNewCategory.htm"/>
    <TocEntry Title="Edit Shop Categories" Link="/Content/GlobalIchiba_RMSManual/ShopManual/ShopCategories/EditCategories.htm"/>
    <TocEntry Title="Delete Shop Categories" Link="/Content/GlobalIchiba_RMSManual/ShopManual/ShopCategories/DeleteCategories.htm"/>
  </TocEntry>
  <TocEntry Title="Design Shop Pages" Link="/Content/GlobalIchiba_RMSManual/ShopManual/PageDesign/PageDesignTOC.htm">
    <TocEntry Title="Introduction to Shop Pages" Link="/Content/GlobalIchiba_RMSManual/ShopManual/PageDesign/PageDesign.htm" conditions="inc-feat.standardStorefront"/>
    <TocEntry Title="Design Common Template" Link="/Content/GlobalIchiba_RMSManual/ShopManual/PageDesign/DesignCommonTemplate.htm" conditions="inc-feat.standardStorefront"/>
    <TocEntry Title="Design Shop Top Page" Link="/Content/GlobalIchiba_RMSManual/ShopManual/PageDesign/DesignShopTopPage.htm" conditions="inc-feat.standardStorefront"/>
    <TocEntry Title="Design Navigation Bar" Link="/Content/GlobalIchiba_RMSManual/ShopManual/PageDesign/DesignNavigationBar.htm" conditions="inc-feat.standardStorefront"/>
    <TocEntry Title="Design Product Pages" Link="/Content/GlobalIchiba_RMSManual/ShopManual/PageDesign/DesignProductPage.htm" conditions="inc-feat.standardStorefront"/>
    <TocEntry Title="Design Mobile Pages" Link="/Content/GlobalIchiba_RMSManual/ShopManual/PageDesign/DesignMobilePages.htm"/>
    <TocEntry Title="Position Video Widgets" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Products/PositionVideoWidgets.htm" conditions="inc-feat.standardStorefront"/>
    <TocEntry Title="Rakuten Gold" conditions="inc-feat.rakutenGold" Link="/Content/GlobalIchiba_RMSManual/ShopManual/PageDesign/RakutenGold.htm"/>
    <TocEntry Title="Media Library" conditions="inc-feat.mediaLibrary" Link="/Content/GlobalIchiba_RMSManual/ShopManual/PageDesign/MediaLibrary.htm"/>
    <TocEntry Title="Manage Product Inventories Across Shops" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Products/JapanInventory.htm" conditions="inc-feat.xBorderToJapan"/>
    <TocEntry Title="Product Validation Requirements for Japan" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Products/JapanValidation.htm" conditions="inc-feat.xBorderToJapan"/>
    <TocEntry Title="Synchronise Product Information Across Shops" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Products/XBorderProductSync.htm" conditions="inc-feat.xBorderToJapan"/>
  </TocEntry>
  <TocEntry Title="Order Management" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/OrderManagementTOC.htm">
    <TocEntry Title="Introduction to Order Management" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/Orders.htm"/>
    <TocEntry Title="Required Actions for Each Order Status" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/OrderStatuses/OrderStatuses.htm"/>
    <TocEntry Title="Watch Orders" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/ManageOrders/WatchOrders.htm"/>
    <TocEntry Title="View Order Details" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/ManageOrders/OrderDetails.htm"/>
    <TocEntry Title="Update Order Labels" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/ManageOrders/UpdateOrderLabels.htm"/>
    <TocEntry Title="Update Order Labels for Multiple Orders" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/ManageOrders/UpdateOrderLabelsMultipleOrders.htm"/>
    <TocEntry Title="Manage Labels" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/ManageOrders/ManageLabels.htm"/>
    <TocEntry Title="Print an Invoice" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/ManageOrders/PrintAnInvoice.htm"/>
    <TocEntry Title="Ship Orders" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/ManageOrders/ShipOrders.htm"/>
    <TocEntry Title="Update Tracking Numbers" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/ManageOrders/UpdateTrackingNumbers.htm"/>
    <TocEntry Title="Schedule Shipping Dates" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/ManageOrders/ScheduleShippingDates.htm" conditions="inc-feat.shippingLabels"/>
    <TocEntry Title="Required Actions for Carrier Shipping Statuses" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/OrderStatuses/ShippingStatuses.htm" conditions="inc-feat.shippingCarrierStatuses"/>
    <TocEntry Title="Print Shipping Labels" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/ManageOrders/PrintShippingLabels.htm" conditions="inc-feat.shippingLabels"/>
    <TocEntry Title="Cancel Orders" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/ManageOrders/CancelOrders.htm"/>
    <TocEntry Title="Refund Orders" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/ManageOrders/IssueRefunds.htm"/>
    <TocEntry Title="Enter Shipping Fees" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/ShippingFees.htm"/>
    <TocEntry Title="Edit Shipping Method" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/ManageOrders/EditShippingMethod.htm"/>
    <TocEntry Title="Edit Shipping Address" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/ManageOrders/editShippingAddress.htm"/>
    <TocEntry Title="Edit Payment Method" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/ManageOrders/editPaymentMethod.htm" conditions="inc-feat.TW_EditShippingFee"/>
    <TocEntry Title="Edit Order Total" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/ManageOrders/EditOrderTotal.htm"/>
    <TocEntry Title="Additional Product Discounts" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/ManageOrders/AdditionalProductDiscounts.htm"/>
    <TocEntry Title="Automatic Order [%=UITextNames_main.SMSNotifications%]" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/ListOfEmails.htm"/>
    <TocEntry Title="Email Customers" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Emails.htm"/>
    <TocEntry Title="SMS Notification History" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/ManageOrders/SMSNotification.htm" conditions="inc-feat.SMSNotifications"/>
    <TocEntry Title="Create Order Email Templates" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/EmailTemplates.htm"/>
    <TocEntry Title="Download Order Data" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/DownloadOrderData.htm" conditions="display.ScreenOnly"/>
    <TocEntry Title="Bulk Order Updates" Link="/Content/GlobalIchiba_RMSManual/Appendices/BulkOrder.htm" conditions="display.ScreenOnly"/>
  </TocEntry>
  <TocEntry Title="Marketing Campaigns" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Marketing/MarketingTOC.htm">
    <TocEntry Title="Introduction to Marketing Campaigns" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Marketing/Campaigns.htm"/>
    <TocEntry Title="Product Discount Campaigns" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Marketing/CreateAProductDiscount.htm" conditions="inc-feat.campaignProductDiscount"/>
    <TocEntry Title="Private Sale Campaigns" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Marketing/CreateAPrivateSaleCampaign.htm" conditions="inc-feat.campaignPrivateSale"/>
    <TocEntry Title="Order Discount Campaigns" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Marketing/CreateAnOrderDiscount.htm"/>
    <TocEntry Title="Shipping Discount Campaigns" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Marketing/CreateAShippingDiscount.htm" conditions="inc-feat.campaignShippingDiscount"/>
    <TocEntry Title="Point Reward Campaigns" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Marketing/CreateAPointRewardCampaign.htm" conditions="inc-feat.campaignPointReward"/>
    <TocEntry Title="Shop Voucher Campaigns" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Marketing/CreateAShopCouponCampaign.htm" conditions="inc-feat.campaignShopCoupon"/>
    <TocEntry Title="Buy One Get One Campaigns" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Marketing/CreateABuyOneGetOneCampaign.htm" conditions="inc-feat.campaignBuyOneGetOne"/>
    <TocEntry Title="Bundle Discount Campaigns" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Marketing/CreateABundleDiscountCampaign.htm"/>
    <TocEntry Title="Edit Campaigns" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Marketing/EditCampaign.htm"/>
    <TocEntry Title="Delete Campaigns" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Marketing/DeleteCampaign.htm"/>
    <TocEntry Title="Ads" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Marketing/Ads.htm" conditions="inc-feat.ads"/>
  </TocEntry>
  <TocEntry Title="[%=UITextNames_main.rmail%]" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Marketing/RMailTOC.htm" conditions="inc-feat.rMail_on" ReplaceMergeNode="false">
    <TocEntry Title="Introduction to [%=UITextNames_main.rmail%]" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Marketing/RMail.htm" conditions="inc-feat.rMail_on"/>
    <TocEntry Title="[%=UITextNames_main.rmail%] Statuses" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Marketing/RMailStatuses.htm" conditions="inc-feat.rMail_on"/>
    <TocEntry Title="Mailing Lists" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Marketing/MailingLists.htm" conditions="inc-feat.rMail_on"/>
    <TocEntry Title="Create Mailing Lists" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Marketing/MailingListsCreate.htm" conditions="inc-feat.rMail_on"/>
    <TocEntry Title="Edit Mailing Lists" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Marketing/MailingListsEdit.htm" conditions="inc-feat.rMail_on"/>
    <TocEntry Title="Compose [%=UITextNames_main.rmail%]" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Marketing/ComposeEditRMail.htm" conditions="inc-feat.rMail_on"></TocEntry>
    <TocEntry Title="Schedule [%=UITextNames_main.rmail%]" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Marketing/ScheduleRMail.htm" conditions="inc-feat.rMail_on"/>
    <TocEntry Title="Cancel Scheduled [%=UITextNames_main.rmail%]" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Marketing/CancelRMail.htm" conditions="inc-feat.rMail_on"/>
    <TocEntry Title="Edit [%=UITextNames_main.rmail%]" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Marketing/EditRMail.htm" conditions="inc-feat.rMail_on"/>
    <TocEntry Title="Delete [%=UITextNames_main.rmail%]" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Marketing/DeleteRMail.htm" conditions="inc-feat.rMail_on"/>
  </TocEntry>
  <TocEntry Title="Reports" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Reports/ReportsTOC.htm">
    <TocEntry Title="Introduction to Reports" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Reports/Reports.htm"/>
    <TocEntry Title="Shop Sales Report" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Reports/ShopSalesReport.htm"/>
    <TocEntry Title="Product Sales Report" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Reports/ProductSalesReport.htm"/>
    <TocEntry Title="Product Ranking Report" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Reports/ProductRankingReport.htm"/>
    <TocEntry Title="Category Ranking Report" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Reports/CategoryRankingReport.htm"/>
  </TocEntry>
  <TocEntry Title="Settings" Link="/Content/GlobalIchiba_RMSManual/ShopManual/SettingsTOC.htm">
    <TocEntry Title="Manage Company Information" Link="/Content/GlobalIchiba_RMSManual/ShopManual/CompanyInformation/CompanyInformation.htm"/>
    <TocEntry Title="Manage Shop Information" Link="/Content/GlobalIchiba_RMSManual/ShopManual/ShopInformation/ShopInformation.htm"/>
    <TocEntry Title="Checkout Messages" Link="/Content/GlobalIchiba_RMSManual/ShopManual/AdditionalCheckoutOptions/AdditionalCheckoutOptions.htm"/>
  </TocEntry>
  <TocEntry Title="Shop Management" Link="/Content/GlobalIchiba_RMSManual/ShopManual/ShopManagement/ShopManagementTOC.htm">
    <TocEntry Title="Introduction to Multiple Shop Management" Link="/Content/GlobalIchiba_RMSManual/ShopManual/ShopManagement/sellInOtherMarketplaces.htm"/>
    <TocEntry Title="Open a Shop in Another Rakuten Marketplace" Link="/Content/GlobalIchiba_RMSManual/ShopManual/ShopManagement/openNewShop.htm"/>
    <TocEntry Title="Product Catalogue Translation Options" Link="/Content/GlobalIchiba_RMSManual/ShopManual/ShopManagement/productTranslationOptions.htm"/>
    <TocEntry Title="Open Shop Wizard: Shipping Options" Link="/Content/GlobalIchiba_RMSManual/ShopManual/ShopManagement/shippingOptions-wizard.htm"/>
    <TocEntry Title="Shop Management Page" Link="/Content/GlobalIchiba_RMSManual/ShopManual/ShopManagement/shopManagement.htm"/>
  </TocEntry>
  <TocEntry Title="Administration" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Administration/Administration.htm">
    <TocEntry Title="Add User" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Administration/AddUser.htm"/>
    <TocEntry Title="Edit User" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Administration/EditUser.htm"/>
    <TocEntry Title="Delete User" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Administration/DeleteUser.htm"></TocEntry>
    <TocEntry Title="Manage Account Information" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Administration/AccountInformation.htm"/>
    <TocEntry Title="View Payment Transactions" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Administration/PaymentTransactions.htm"/>
  </TocEntry>
  <TocEntry Title="Messages To and From Customers" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/CustomerMessages.htm"></TocEntry>
  <TocEntry Title="Using HTML in RMS" Link="/Content/GlobalIchiba_RMSManual/Appendices/HTMLTags/UsingHTMLinRMSgTOC.htm">
    <TocEntry Title="Allowed HTML Tags" Link="/Content/GlobalIchiba_RMSManual/Appendices/HTMLTags/AllowedHTMLTags.htm"/>
    <TocEntry Title="Disallowed HTML Tags" Link="/Content/GlobalIchiba_RMSManual/Appendices/HTMLTags/DisallowedHTMLTags.htm"/>
  </TocEntry>
  <TocEntry Title="Bulk Operations - Using CSV Files" Link="/Content/GlobalIchiba_RMSManual/Appendices/BulkOperationsTOC.htm">
    <TocEntry Title="Bulk Product Updates" Link="/Content/GlobalIchiba_RMSManual/Appendices/CreatingCSVFiles/BulkProduct.htm"/>
    <TocEntry Title="Download Products Data" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Products/DownloadProductCSV.htm"/>
    <TocEntry Title="List of Product CSV File Fields" Link="/Content/GlobalIchiba_RMSManual/Appendices/CreatingCSVFiles/AllProductCSVFields.htm"/>
    <TocEntry Title="Introduction to Creating Product CSV Files" Link="/Content/GlobalIchiba_RMSManual/Appendices/CreatingCSVFiles/CreatingCSVFiles.htm"/>
    <TocEntry Title="Create a Product CSV File to Add New Products" Link="/Content/GlobalIchiba_RMSManual/Appendices/CreatingCSVFiles/CreateCSVAdd.htm"/>
    <TocEntry Title="Create a Product CSV File to Edit Existing Products" Link="/Content/GlobalIchiba_RMSManual/Appendices/CreatingCSVFiles/CreateCSVEdit.htm"/>
    <TocEntry Title="Create a Product CSV File to Update Inventory" Link="/Content/GlobalIchiba_RMSManual/Appendices/CreatingCSVFiles/CreateCSVInventory.htm"/>
    <TocEntry Title="Create a Product CSV File to Delete Products" Link="/Content/GlobalIchiba_RMSManual/Appendices/CreatingCSVFiles/CreateCSVDelete.htm"/>
    <TocEntry Title="Upload a Product CSV File" Link="/Content/GlobalIchiba_RMSManual/ShopManual/Products/UploadAProductCSVFile.htm"/>
    <TocEntry Title="List of Rakuten Category Codes" Link="/Content/GlobalIchiba_RMSManual/Appendices/RakutenCategories/GermanyCategories.htm" conditions="inc-mp.DE"/>
    <TocEntry Title="List of Rakuten Category Codes" Link="/Content/GlobalIchiba_RMSManual/Appendices/RakutenCategories/IndonesiaCategories.htm" conditions="inc-mp.ID"/>
    <TocEntry Title="List of Rakuten Category Codes" Link="/Content/GlobalIchiba_RMSManual/Appendices/RakutenCategories/MalaysiaCategories.htm" conditions="inc-mp.MY"/>
    <TocEntry Title="List of Rakuten Category Codes" Link="/Content/GlobalIchiba_RMSManual/Appendices/RakutenCategories/SingaporeCategories.htm" conditions="inc-mp.SG"/>
    <TocEntry Title="List of Rakuten Category Codes" Link="/Content/GlobalIchiba_RMSManual/Appendices/RakutenCategories/SpainCategories.htm" conditions="inc-mp.ES"/>
    <TocEntry Title="List of Rakuten Category Codes" Link="/Content/GlobalIchiba_RMSManual/Appendices/RakutenCategories/TaiwanCategories.htm" conditions="inc-mp.TW"/>
    <TocEntry Title="List of Rakuten Category Codes" Link="/Content/GlobalIchiba_RMSManual/Appendices/RakutenCategories/ThailandCategories.htm" conditions="inc-mp.TH"/>
    <TocEntry Title="List of Rakuten Category Codes" Link="/Content/GlobalIchiba_RMSManual/Appendices/RakutenCategories/UKCategories.htm" conditions="inc-mp.UK"/>
    <TocEntry Title="List of Product Labels" Link="/Content/GlobalIchiba_RMSManual/Appendices/ProductLabels.htm" conditions="inc-mp.UK"/>
    <TocEntry Title="List of Energy Class Identification Numbers for Product CSV Files" Link="/Content/GlobalIchiba_RMSManual/Appendices/CreatingCSVFiles/ListOfEnergyClassIDs.htm"/>
    <TocEntry Title="Bulk Order Updates" Link="/Content/GlobalIchiba_RMSManual/Appendices/BulkOrder.htm"/>
    <TocEntry Title="Download Order Data" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/DownloadOrderData.htm"/>
    <TocEntry Title="List of Order CSV File Fields" Link="/Content/GlobalIchiba_RMSManual/Appendices/ListofOrderCSVFields.htm"/>
    <TocEntry Title="Ship Multiple Orders" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/ManageOrders/ShipMultipleOrders.htm"/>
    <TocEntry Title="Update Tracking Numbers for Multiple Orders" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/ManageOrders/UpdateMultipleTrackingNumbers.htm"/>
    <TocEntry Title="Ship Multiple Orders and Update Tracking Numbers" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/ManageOrders/ShipAndUpdateTracking.htm"/>
    <TocEntry Title="Cancel Multiple Orders" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/ManageOrders/CancelMultipleOrders.htm"/>
    <TocEntry Title="Schedule Shipping Dates for Multiple Orders" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/ManageOrders/ScheduleShippingDatesMultiple.htm" conditions="inc-feat.shippingLabels"/>
    <TocEntry Title="Print Shipping Labels for Multiple Orders" Link="/Content/GlobalIchiba_RMSManual/OrdersManual/Orders/ManageOrders/DownloadMultipleLabels.htm" conditions="inc-feat.shippingLabels"/>
  </TocEntry>
  <TocEntry Title="FAQ" Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/000_FAQTOC.htm" conditions="inc-feat.TWFAQ">
    <TocEntry
      Title="RMSg主畫面"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/00_MainMenu.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="訂單_訂單"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/01_Orders_Orders.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="訂單_管理郵件範本"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/02_Orders_EmailTemplates.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="訂單_顧客訊息"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/03_Orders_CustomerMessages.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="商品目錄_商品"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/05_Products_Products.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="商品目錄_店家類別"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/06_Products_ShopCategories.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="商品目錄_精選商品"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/07_Products_Featured.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="頁面設計_設計常用範本"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/08_PageDesign_Common.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="教學網址如下"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/09_Teaching_Old_System_Running_Board.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="頁面設計_設計店家首頁"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/10_PageDesign_ShopTop.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="頁面設計_設計商品頁面"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/11_PageDesign_ProductPage.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="頁面設計_設計商品頁面"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="頁面設計_媒體櫃"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/12_Media_Library.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="市場行銷_R-Mail"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/14_Marketing_RMail.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="報告_店家銷售額"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/15_Reports_ShopSales.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="報告_商品銷售"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/16_Reports_ProductSales.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="報告_商品排名"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/17_Reports_ProductRanking.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="報告_類別排名"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/18_Reports_CategoryRanking.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="設定_店家資訊"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/19_Settings_ShopInfo.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="【範本】退 換貨說明"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/19-1_Refund_Instructions.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="◎配送方式說明（建議版本）："
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/19-2_Delivery.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="設定_配送選項"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/20_Settings_ShippingOptions.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="金流說明"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/20-1_Settings_PaymentOptions.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="信用卡設定"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/21_Settings_CreditCard.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="信用卡申請"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/22_Application_CreditCard.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="貨到付款申請"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/22-1_Application_Shipping_7-11.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="黑貓貨到付款申請"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/22-2_Application_Shipping_BlackCat.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="醫療器材申請"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/22-3_Application_MedicalEquipment.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="經營權移轉"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/22-4_Application_ManagementTransfer.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="其它申請文件"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/22-5_Applications_Other.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="管理者_使用者"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/23_Administration_Users.htm"
      conditions="inc-feat.TWFAQ"></TocEntry>
    <TocEntry
      Title="管理者_帳號資訊"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/24_Administration_AccountInfo.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="登入資訊"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/25_Login.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="認證登入教學"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/25-1_LoginAuthentication.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="金財通"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/26_JinChoiTong.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="點數、對帳"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/27_Points.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="申請開店"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/28_Application_Shop.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="報名操作課程"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/29_Course_Registration.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="CSV(商品. 訂單)"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/30_Order_CSV.htm"
      conditions="inc-feat.TWFAQ" />
    <TocEntry
      Title="媒體櫃(Gold)"
      Link="/Content/GlobalIchiba_RMSManual/Appendices/TW_FAQ/31_RakutenGold.htm"
      conditions="inc-feat.TWFAQ" />
  </TocEntry>
</CatapultToc>